const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const employeeSchema = new Schema(
  {
    id: {
      type: Number,
    },
    employee_name: {
      type: String,
    },
    employee_salary: {
      type: Number,
    },
    employee_age: {
      type: Number,
    },
    // profile_image: {
    //   type: String,
    //   default: '',
    // },
  },
  { timestamps: true }
);

module.exports = mongoose.model('employee', employeeSchema);
