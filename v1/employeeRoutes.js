const express = require('express');
const controller = require('./employeeController');

const router = express.Router();

// ALL CURD operations
router.post('/employee/create', controller.insert);

router.get('/employee/getAll', controller.getAll);

router.get('/employee/get/:id', controller.getById);

router.put('/employee/update/:id', controller.update);

router.delete('/employee/delete/:id', controller.delete);

module.exports = router;
