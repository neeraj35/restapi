const { json } = require('body-parser');
const Employee = require('../models/employee');

exports.insert = async (req, res, next) => {
  try {
    const exists = await Employee.findOne({ id: req.body.id });
    if (exists) {
      return res.status(400).json({ message: 'User Id must be unique! Try Again!' });
    }
    const result = await new Employee(req.body).save();

    res.status(201).json({ message: 'Employee Added Successfully!', data: result });
  } catch (error) {
    console.log(error);
  }
};

exports.getById = async (req, res, next) => {
  try {
    const employee = await Employee.findOne({ id: req.params.id });
    if (!employee) {
      return res.status(400).json({ message: 'Employee not found!', data: [] });
    }

    res.status(200).json({ message: 'Success', data: employee });
  } catch (error) {
    console.log(error);
  }
};

exports.getAll = async (req, res, next) => {
  try {
    const allEmployees = await Employee.find();
    res.status(200).json({ message: 'Success', allEmployees: allEmployees });
  } catch (error) {}
};

exports.update = async (req, res, next) => {
  try {
    const employee = await Employee.findOne({ id: req.params.id });
    if (!employee) {
      return res.status(400).json({ message: 'Employee Id is not exists!' });
    }
    // const { employee_name, employee_salary, employee_age } = req.body;
    const result = await Employee.findOneAndUpdate({ id: req.params.id }, req.body, {
      new: true,
    });
    res.status(200).json({ message: 'Data Updated', data: result });
  } catch (error) {}
};

exports.delete = async (req, res, next) => {
  try {
    const count = await Employee.deleteOne({ id: req.params.id });
    if (!count) {
      return res.status(400).json({ message: 'Invalid Employee Id' });
    }
    res.json({ message: 'Employee Deleted Successfully!', data: count });
  } catch (error) {}
};
