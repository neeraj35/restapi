const express = require('express');
const mongoose = require('mongoose');
const config = require('config');
const bodyParser = require('body-parser');
const http = require('http');
const cors = require('cors');

const MONGO_URI = config.get('mongoDBUrl');
const PORT = config.get('port');

const app = express();
const server = http.createServer(app);

const route = require('./v1/employeeRoutes');

//Middlewares
app.use(cors());
app.use(bodyParser.json());
app.use((req, res, next) => {
  const info = req.method + ' ' + res.statusCode + ' ' + req.url;
  console.log('API HIT ------->', info, '\n|\n|');
  // if (req.header('lang') == 'en' || req.header('lang') == '') {
  //   req.lang = 'en';
  // } else {
  //   req.lang = req.header('lang');
  //   console.log('Request Lang', req.lang);
  // }
  next();
});

app.use('/api/v1', route);

// Error Middleware
app.use((error, req, res, next) => {
  console.log('Error Midleware', error);
});

server.listen(PORT, () => {
  console.log('======PORT - ' + PORT + '========');
});
mongoose
  .connect(MONGO_URI, {
    useUnifiedTopology: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useCreateIndex: true,
  })
  .then((result) => {
    console.log('=====Connected to MongoDB=====');
  })
  .catch((err) => {
    throw new Error('MongoDB Connection Error!');
  });
